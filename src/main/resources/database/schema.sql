CREATE SCHEMA IF NOT EXISTS bank;

DROP TABLE IF EXISTS bank.account;
DROP TABLE IF EXISTS bank.money_transfer;


CREATE TABLE bank.account
(
    id      bigint         NOT NULL PRIMARY KEY,
    balance NUMERIC(10, 3) NOT NULL
);

CREATE TABLE bank.money_transfer
(
    id     bigint         NOT NULL PRIMARY KEY AUTO_INCREMENT,
    source bigint         NOT NULL REFERENCES bank.account (id),
    target bigint         NOT NULL REFERENCES bank.account (id),
    amount NUMERIC(10, 3) NOT NULL,
    status VARCHAR(255) NOT NULL
);
ALTER TABLE bank.money_transfer CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
