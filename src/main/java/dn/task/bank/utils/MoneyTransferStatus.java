package dn.task.bank.utils;

public enum MoneyTransferStatus {
    Проведен,
    Ошибка,
    Обрабатывается;
}
