package dn.task.bank.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InsufficientBalanceException extends Exception{
    public InsufficientBalanceException(String errorMessage) {
        super(errorMessage);
    }
}
