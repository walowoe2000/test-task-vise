package dn.task.bank.schedule;

import dn.task.bank.model.MoneyTransfer;
import dn.task.bank.utils.MoneyTransferStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

@Component
@Slf4j
public class UpdateStatusSchedule {

    private RestTemplate restTemplate;

    private final String resourceUrl = "http://localhost:9966/payment";
    @Autowired
    public UpdateStatusSchedule(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Scheduled(fixedDelay = 10 * 1000, initialDelay = 30 * 1000)
    public void updateStatus(){
        log.info("IN UpdateStatusSchedule updateStatus");
        ResponseEntity<MoneyTransfer[]> response =
                restTemplate.getForEntity(resourceUrl, MoneyTransfer[].class);
        MoneyTransfer[] moneyTransfers = response.getBody();
        if(moneyTransfers != null) {
            for (int i = 0; i < moneyTransfers.length; i++) {
                while (moneyTransfers[i].getStatus().equals(MoneyTransferStatus.Обрабатывается)) {
                    moneyTransfers[i].setStatus(MoneyTransferStatus.values()[
                            (new Random()).nextInt(MoneyTransferStatus.values().length)]);
                }
                restTemplate.put(resourceUrl, moneyTransfers[i]);
            }
            log.info("IN UpdateStatusSchedule updateStatus updated: {}", moneyTransfers.length);
        }
    }
}
