package dn.task.bank.repository;

import dn.task.bank.model.MoneyTransfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MoneyTransferRepository extends JpaRepository <MoneyTransfer, Long> {
    @Query("SELECT t FROM MoneyTransfer t WHERE t.status = 'Обрабатывается'")
    List<MoneyTransfer> getUnprocessedTransfers();
}
