package dn.task.bank.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MoneyTransferNotFoundException extends Exception {
    public MoneyTransferNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
