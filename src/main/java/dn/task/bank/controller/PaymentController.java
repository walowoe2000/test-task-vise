package dn.task.bank.controller;

import dn.task.bank.exceptions.AccountNotFoundException;
import dn.task.bank.model.MoneyTransfer;
import dn.task.bank.service.MoneyTransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    MoneyTransferService moneyTransferService;

    @Autowired
    public PaymentController(MoneyTransferService moneyTransferService) {
        this.moneyTransferService = moneyTransferService;
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public List<MoneyTransfer> getUnprocessedMoneyTransfers(){
        return moneyTransferService.getAllUnprocessedTransfers();
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping
    public void updateMoneyTransferStatus(@Validated({MoneyTransfer.Exist.class})@RequestBody MoneyTransfer moneyTransfer) throws AccountNotFoundException {
        moneyTransferService.updateMoneyTransferStatus(moneyTransfer);
    }
}
