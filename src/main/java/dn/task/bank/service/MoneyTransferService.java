package dn.task.bank.service;

import dn.task.bank.exceptions.AccountNotFoundException;
import dn.task.bank.exceptions.InsufficientBalanceException;
import dn.task.bank.exceptions.MoneyTransferNotFoundException;
import dn.task.bank.model.Account;
import dn.task.bank.model.MoneyTransfer;
import dn.task.bank.repository.AccountRepository;
import dn.task.bank.repository.MoneyTransferRepository;
import dn.task.bank.utils.MoneyTransferStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@Slf4j
public class MoneyTransferService {

    AccountRepository accountRepository;
    MoneyTransferRepository moneyTransferRepository;

    @Autowired
    public MoneyTransferService(AccountRepository accountRepository, MoneyTransferRepository moneyTransferRepository) {
        this.accountRepository = accountRepository;
        this.moneyTransferRepository = moneyTransferRepository;
    }

    public String getMoneyTransferStatus(Long id) throws MoneyTransferNotFoundException {
        log.info("IN MoneyTransferService getMoneyTransferStatus {}", id);
        return moneyTransferRepository.findById(id).orElseThrow(() -> new MoneyTransferNotFoundException())
                .getStatus().toString();
    }

    public Long createTransfer(MoneyTransfer moneyTransfer) throws AccountNotFoundException, InsufficientBalanceException {
        log.info("IN MoneyTransferService createTransfer {}", moneyTransfer.toString());
        BigDecimal balance = accountRepository.findById(moneyTransfer.getSource())
                .orElseThrow(() -> new AccountNotFoundException())
                .getBalance();
        accountRepository.findById(moneyTransfer.getTarget())
                .orElseThrow(() -> new AccountNotFoundException());
        if (balance.compareTo(moneyTransfer.getAmount()) >= 0) {
            return moneyTransferRepository.save(moneyTransfer).getId();
        } else {
            throw new InsufficientBalanceException();
        }
    }

    public List<MoneyTransfer> getAllUnprocessedTransfers() {
        log.info("IN MoneyTransferService getAllUnprocessedTransfers");
        return moneyTransferRepository.getUnprocessedTransfers();
    }

    public void updateMoneyTransferStatus(MoneyTransfer moneyTransfer) throws AccountNotFoundException {
        log.info("IN MoneyTransferService updateMoneyTransferStatus {}", moneyTransfer.toString());
        if (moneyTransfer.getStatus().equals(MoneyTransferStatus.Проведен)) {
            Account source = accountRepository.findById(moneyTransfer.getSource())
                    .orElseThrow(() -> new AccountNotFoundException());
            Account target = accountRepository.findById(moneyTransfer.getTarget())
                    .orElseThrow(() -> new AccountNotFoundException());
            BigDecimal amount = moneyTransfer.getAmount();
            source.debitBalance(amount);
            target.addToBalance(amount);
            accountRepository.save(source);
            accountRepository.save(target);
        }
        moneyTransferRepository.save(moneyTransfer);
    }
}
