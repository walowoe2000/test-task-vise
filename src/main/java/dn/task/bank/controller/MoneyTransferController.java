package dn.task.bank.controller;

import dn.task.bank.exceptions.AccountNotFoundException;
import dn.task.bank.exceptions.InsufficientBalanceException;
import dn.task.bank.exceptions.MoneyTransferNotFoundException;
import dn.task.bank.model.MoneyTransfer;
import dn.task.bank.service.MoneyTransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transfer")
public class MoneyTransferController {

    MoneyTransferService moneyTransferService;

    @Autowired
    public MoneyTransferController(MoneyTransferService moneyTransferService) {
        this.moneyTransferService = moneyTransferService;
    }

    @GetMapping(value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    String getStatus(@PathVariable Long id) throws MoneyTransferNotFoundException {
            return moneyTransferService.getMoneyTransferStatus(id);
    }

    @PostMapping(value = "/create",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Long createTransfer(@RequestBody @Validated({MoneyTransfer.New.class}) MoneyTransfer moneyTransfer)
            throws InsufficientBalanceException, AccountNotFoundException {
        return moneyTransferService.createTransfer(moneyTransfer);
    }
}
