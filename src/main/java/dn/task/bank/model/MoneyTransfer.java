package dn.task.bank.model;

import dn.task.bank.utils.MoneyTransferStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
public class MoneyTransfer {

    public interface New {
    }

    public interface Exist {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Null(groups = New.class)
    @NotNull(groups = Exist.class)
    private Long id;

    @NotNull
    @DecimalMin("0.1")
    @DecimalMax("9999.99")
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    MoneyTransferStatus status = MoneyTransferStatus.Обрабатывается;

    @NotNull
    private Long source;

    @NotNull
    private Long target;
}
