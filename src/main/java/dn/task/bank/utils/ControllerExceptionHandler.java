package dn.task.bank.utils;

import dn.task.bank.exceptions.AccountNotFoundException;
import dn.task.bank.exceptions.InsufficientBalanceException;
import dn.task.bank.exceptions.MoneyTransferNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;

@ControllerAdvice
public class ControllerExceptionHandler {
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({MoneyTransferNotFoundException.class, AccountNotFoundException.class})
    public void notFoundHandleException() {
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({InsufficientBalanceException.class, ValidationException.class, IllegalArgumentException.class})
    public void insufficientBalanceHandleException() {
    }
}
