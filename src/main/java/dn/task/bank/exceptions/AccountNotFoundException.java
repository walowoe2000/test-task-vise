package dn.task.bank.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AccountNotFoundException extends Exception{
    public AccountNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
