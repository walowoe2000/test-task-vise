package dn.task.bank.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;


@Entity
@Data
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private BigDecimal balance;

    public void addToBalance(BigDecimal amount) {
        this.balance = this.balance.add(amount);
    }
    public void debitBalance(BigDecimal amount) {
        this.balance = this.balance.subtract(amount);
    }
}
