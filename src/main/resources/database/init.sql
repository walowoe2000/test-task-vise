INSERT INTO bank.account (id, balance)
VALUES (1214, 10000.78);
INSERT INTO bank.account (id, balance)
VALUES (5254, 4000.00);
INSERT INTO bank.account (id, balance)
VALUES (1231, 33.50);
INSERT INTO bank.account (id, balance)
VALUES (1111, 550.00);

INSERT INTO bank.money_transfer (id, amount, source, target, status)
VALUES (5555, 99, 5254, 1111,'Обрабатывается');

INSERT INTO bank.money_transfer (id, amount, source, target, status)
VALUES (1010, 10, 1214, 1111,'Обрабатывается');

INSERT INTO bank.money_transfer (id, amount, source, target, status)
VALUES (1012, 3.50, 1231, 1111,'Обрабатывается');